#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "apply_pixel_transformation.h"
#include "blur.h"
#include "bmp_info.h"
#include "bmp_process.h"
#include "custom.h"
#include "pixel_func.h"
#include "sharpen.h"

static const char help_message[] =
    "argv[1] - transformation name (blur, sharpen, custom, negate, grayscale)\n"
    "argv[2] - transformation coefficient if argv[1] is 'blur' or 'sharpen',\n"
    "          custom transformation matrix .txt file name,\n"
    "          which should contain matrix size and square matrix, if 'custom',\n"
    "          otherwise - any non-empty string\n"
    "argv[3] - source .bmp file name\n"
    "argv[4] - result .bmp file name\n"
    "Attention!!!\n"
    "Only Big-Endian pixel byte order is supported yet (in RGBA 'R' is the lowest),\n"
    "compression and channel bitmasks are not supported\n"
    "Only 32-bit-per-pixel format supported";

int main(int argc, char** argv)
{
    int error_happened = 0;
    if (argc == 2 && strcmp(argv[1], "--help") == 0) {
        puts(help_message);
        return 0;
    }
    if (argc < 5) {
        puts("Provided less arguments than needed");
        error_happened = 1;
    } else if (strcmp(argv[1], "blur") != 0 &&
               strcmp(argv[1], "sharpen") != 0 &&
               strcmp(argv[1], "custom") != 0 &&
               strcmp(argv[1], "negate") != 0 &&
               strcmp(argv[1], "grayscale") != 0) {
        puts("Wrong operation name");
        error_happened = 1;
    } else if (access(argv[3], F_OK) == -1) {
        puts("Source file does not exist");
        error_happened = 1;
    } else if (access(argv[3], R_OK) == -1) {
        puts("Source file read permission denied");
        error_happened = 1;
    } else if (strcmp(argv[1], "custom") == 0) {
        if (access(argv[2], F_OK) == -1) {
            puts("Matrix file does not exist");
            error_happened = 1;
        } else if (access(argv[2], R_OK) == -1) {
            puts("Matrix file read permission denied");
            error_happened = 1;
        }
    }
    if (error_happened) {
        puts(help_message);
        return 1;
    }

    struct bmp_info source_info;
    struct bmp_info result_info;
    bmp_initialize(argv[3], argv[4], &source_info, &result_info);

    if (strcmp(argv[1], "blur") == 0) {
        blur(&result_info, &source_info, strtol(argv[2], NULL, 10));
    } else if (strcmp(argv[1], "sharpen") == 0) {
        sharpen(&result_info, &source_info, strtol(argv[2], NULL, 10));
    } else if (strcmp(argv[1], "custom") == 0) {
        FILE* matrix_file = fopen(argv[2], "r");
        custom(&result_info, &source_info, matrix_file);
        fclose(matrix_file);
    } else if (strcmp(argv[1], "negate") == 0) {
        apply_pixel_transformation((void*)result_info.pixel_array,
                                   (void*)source_info.pixel_array,
                                   source_info.width,
                                   source_info.height,
                                   &negate);
    } else if (strcmp(argv[1], "grayscale") == 0) {
        apply_pixel_transformation((void*)result_info.pixel_array,
                                   (void*)source_info.pixel_array,
                                   source_info.width,
                                   source_info.height,
                                   &grayscale);
    }

    bmp_finalize(&source_info, &result_info);
    return 0;
}
