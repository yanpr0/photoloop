#include <stdlib.h>

#include "apply_matrix_transformation.h"
#include "bmp_info.h"

void sharpen(struct bmp_info* result_info,
             struct bmp_info* source_info,
             unsigned int sharpness_coef)
{
    int matrix_dim = 2 * sharpness_coef + 1;
    double** sharp_matrix = calloc(matrix_dim, sizeof(double*));

    for (int i = 0; i < matrix_dim; ++i) {
        sharp_matrix[i] = calloc(matrix_dim, sizeof(double));
        for (int j = 0; j < matrix_dim; ++j) {
            sharp_matrix[i][j] = -1.0;
        }
    }
    sharp_matrix[sharpness_coef][sharpness_coef] = matrix_dim * matrix_dim;

    apply_matrix_transformation(result_info->pixel_array,
                                source_info->pixel_array,
                                source_info->width,
                                source_info->height,
                                sharp_matrix,
                                matrix_dim);

    for (int i = 0; i < matrix_dim; ++i) {
        free(sharp_matrix[i]);
    }
    free(sharp_matrix);
}
