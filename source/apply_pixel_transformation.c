#include <stdint.h>

#include "pixel_func.h"

void apply_pixel_transformation(uint32_t* result_array,
                                uint32_t* source_array,
                                int width,
                                int height,
                                pixel_func func) {
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            result_array[width * i + j] = func(result_array[width * i + j]);
        }
    }
}
