#include <stdlib.h>

#include "apply_matrix_transformation.h"
#include "bmp_info.h"

void blur(struct bmp_info* result_info,
          struct bmp_info* source_info,
          unsigned int blur_coef)
{
    int matrix_dim = 2 * blur_coef + 1;
    double coef = 1.0 / (matrix_dim * matrix_dim);
    double** blur_matrix = calloc(matrix_dim, sizeof(double*));

    for (int i = 0; i < matrix_dim; ++i) {
        blur_matrix[i] = calloc(matrix_dim, sizeof(double));
        for (int j = 0; j < matrix_dim; ++j) {
            blur_matrix[i][j] = coef;
        }
    }

    apply_matrix_transformation(result_info->pixel_array,
                               source_info->pixel_array,
                               source_info->width,
                               source_info->height,
                               blur_matrix,
                               matrix_dim);

    for (int i = 0; i < matrix_dim; ++i) {
        free(blur_matrix[i]);
    }
    free(blur_matrix);
}
