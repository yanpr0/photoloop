#include <fcntl.h>
#include <stdint.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "bmp_info.h"
#include "bmp_header.h"

void bmp_initialize(const char* source_name,
                    const char* result_name,
                    struct bmp_info* source_info,
                    struct bmp_info* result_info)
{
    source_info->fd = open(source_name, O_RDONLY);
    result_info->fd = open(result_name, O_RDWR | O_CREAT | O_TRUNC, 0644);

    int true_size = sizeof(struct bmp_header_part) - 2;
    char header[true_size];
    read(source_info->fd, header, true_size);
    source_info->size = result_info->size = *(int*)(header + 2);
    int offset_to_array = *(int*)(header + 10);
    source_info->width = result_info->width = *(int*)(header + 18);
    source_info->height = result_info->height  = *(int*)(header + 22);

    source_info->map = mmap(
        NULL, source_info->size, PROT_READ, MAP_PRIVATE, source_info->fd, 0);

    write(result_info->fd, source_info->map, source_info->size);
    result_info->map = mmap(NULL, result_info->size, PROT_READ | PROT_WRITE,
                           MAP_SHARED, result_info->fd, 0);

    source_info->pixel_array = (uint8_t*)source_info->map + offset_to_array;
    result_info->pixel_array = (uint8_t*)result_info->map + offset_to_array;
}

void bmp_finalize(struct bmp_info* source_info,
                  struct bmp_info* result_info)
{
    munmap(source_info->map, source_info->size);
    munmap(result_info->map, result_info->size);
    close(source_info->fd);
    close(result_info->fd);
}
