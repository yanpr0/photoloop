int clamp_component(int value) {
    if (value < 0) {
        return 0;
    } else if (value > 255) {
        return 255;
    }
    return value;
}
