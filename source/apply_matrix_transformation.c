#include <stdint.h>

#include "clamp_component.h"

void apply_matrix_transformation(uint8_t* result_array,
                                 uint8_t* source_array,
                                 int width,
                                 int height,
                                 double** transform_matrix,
                                 int matrix_dim) {
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            for (int k = 0; k < 3; ++k) {
                double result = 0;
                double coef_sum = 0;
                for (int y = -matrix_dim / 2; y <= matrix_dim / 2; ++y) {
                    for (int x = -matrix_dim / 2; x <= matrix_dim / 2; ++x) {
                        if (i + y >= 0 && j + x >= 0 &&
                            i + y < height && j + x < width) {
                            result +=
                                transform_matrix[y + matrix_dim / 2]
                                                [x + matrix_dim / 2] *
                                source_array[4 * (width * (i + y) + j + x) + k];
                            coef_sum += 
                                transform_matrix[y + matrix_dim / 2]
                                                [x + matrix_dim / 2];
                        }
                    }
                }
                result_array[4 * (width * i + j) + k] =
                    clamp_component(result / coef_sum);
            }
        }
    }
}
