#pragma once

#ifndef CUSTOM
#define CUSTOM

#include <stdio.h>

#include "bmp_info.h"

void custom(struct bmp_info* result_info,
            struct bmp_info* source_info,
            FILE* matrix_file);

#endif
