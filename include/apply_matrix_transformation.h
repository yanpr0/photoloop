#pragma once

#ifndef APPLY_MATRIX_TRANSFORMATION
#define APPLY_MATRIX_TRANSFORMATION

#include <stdint.h>

void apply_matrix_transformation(uint8_t* result_array,
                                 uint8_t* source_array,
                                 int width,
                                 int height,
                                 double** transform_matrix,
                                 int matrix_dim);

#endif
