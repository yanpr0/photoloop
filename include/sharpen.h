#pragma once

#ifndef SHARPEN
#define SHARPEN

#include "bmp_info.h"

void sharpen(struct bmp_info* result_info,
             struct bmp_info* source_info,
             int sharpness_coef);

#endif
