#pragma once

#ifndef BMP_INFO
#define BMP_INFO

#include <stdint.h>

struct bmp_info {
    int fd;
    int size;
    void* map;
    uint8_t* pixel_array;
    int width;
    int height;
};

#endif
