#pragma once

#ifndef APPLY_PIXEL_TRANSFORMATION
#define APPLY_PIXEL_TRANSFORMATION

#include <stdint.h>

#include "pixel_func.h"

void apply_pixel_transformation(uint32_t* result_array,
                                uint32_t* source_array,
                                int width,
                                int height,
                                pixel_func func);

#endif
